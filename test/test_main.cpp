#include <test_suite.h>

#include <cppunit/Test.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestRunner.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestResult.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/TestListener.h>

using namespace CppUnit;

CPPUNIT_TEST_SUITE_REGISTRATION(JaguarVLMTestSuite);

int main()
{
    TestResult controller;
    TestResultCollector result;
    controller.addListener(&result);

    BriefTestProgressListener progress;
    controller.addListener(&progress);

    TestRunner runner;
    runner.addTest(TestFactoryRegistry::getRegistry().makeTest());
    runner.run(controller);

    return result.wasSuccessful() ? 0 : 1;
}

