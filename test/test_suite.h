#pragma once

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestSuite.h>

class JaguarVLMTestSuite : public CPPUNIT_NS::TestFixture {
private:
    CPPUNIT_TEST_SUITE(JaguarVLMTestSuite); // register all unit test cases below

    CPPUNIT_TEST(testFunction);
    CPPUNIT_TEST(testFunction2);

    CPPUNIT_TEST_SUITE_END(); // end of registered unit test list

public:
    void setUp();
    void tearDown();

    void testFunction();
    void testFunction2();
};
