#pragma once

constexpr auto constexpr_factorial(int num) -> int
{
    if (num < 2) {
        return 1;
    }

    return num * constexpr_factorial(num - 1);
}
