# Jaguar Virtual Light Machine
A creator tool for producing visualizations for audio files. In the style of the Virtual Light Machine software made for the Jaguar console.

## Depdendencies
### Windows
On Windows, install [MSYS2](https://www.msys2.org/wiki/MSYS2-installation/) and follow their setup
steps. *After updating your installation, make sure to launch the UCRT64 environment - and **NOT** any other!* Then install the following packages:
```bash
pacman -S mingw-w64-ucrt-x86_64-cmake \
          mingw-w64-ucrt-x86_64-ccache \
          mingw-w64-ucrt-x86_64-gcc \
          mingw-w64-ucrt-x86_64-toolchain \
          mingw-w64-ucrt-x86_64-clang-tools-extra \
          mingw-w64-ucrt-x86_64-ninja \
          mingw-w64-ucrt-x86_64-ffmpeg \
          mingw-w64-ucrt-x86_64-glew \
          mingw-w64-ucrt-x86_64-gtkmm \
          mingw-w64-ucrt-x86_64-libsndfile \
          git

# optional is mingw-w64-ucrt-x86_64-cppcheck
# maybe a text editor while you're at it
```

### Linux
With your package manager, install the following system dependencies:
- cppunit
- clang-tools-extra
- FFmpeg
- glew-2.2.0
- gtkmm-3.24
- libsndfile
- cppcheck (optional)

# Building
Note this project makes use of clang, clang-tools, CPM, Catch2
Depdendencies are automatically imported and built before this project.
```bash
git clone git@gitlab.com:jsfer/jaguar_vlm
cd jaguar_vlm
mkdir build
cd build
cmake .. -GNinja
ninja
```


# Contributing
Before submitting an MR, squash your commits. Then, run your patch through tests, static analysis, and formatter.
```bash
ninja
ninja test
ninja tidy
ninja format
```
