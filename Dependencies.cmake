# Done as a function so that updates to variables like
# CMAKE_CXX_FLAGS don't propagate out to other
# targets
function(myproject_setup_dependencies)

    # Local system dependencies
    find_package(PkgConfig)
    pkg_check_modules(CPPUNIT REQUIRED cppunit)
    pkg_check_modules(GTKMM REQUIRED gtkmm-3.0)
    pkg_check_modules(GLEW REQUIRED glew)
    pkg_check_modules(SNDFILE REQUIRED sndfile)

    pkg_check_modules(FFMPEG REQUIRED IMPORTED_TARGET
        libavdevice
        libavfilter
        libavformat
        libavcodec
        libswresample
        libswscale
        libavutil)




endfunction()
