#include <important_function.h>
#include <iostream>

// ffmpeg libs
#include <libavutil/frame.h>
#include <libavutil/mem.h>
#include <libavcodec/avcodec.h>

#include <GL/glew.h>

#include <gtkmm.h>

#include <sndfile.h>

auto main() -> int
{
    std::cout << super_important_function() << '\n';
}
