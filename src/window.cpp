#include <iostream>

namespace Jaguar {

inline auto something() -> void
{
	std::cout << "hi\n";
}

} // namespace Jaguar
